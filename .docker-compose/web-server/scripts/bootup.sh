#!/bin/bash
printf "\033[0;32m > Bootup ...\n"

if [ -f /docker/initialized ]; then
   rm /docker/initialized
fi

if [[ "${DEBUG:?}" == "true" ]]; then
    ## Debug Mode
    printf "\033[0;32m > Installing XDebug ...\n"

    ( \
        cd / \
        && wget https://github.com/xdebug/xdebug/archive/XDEBUG_2_5_5.zip \
        && unzip XDEBUG_2_5_5.zip \
        && cd XDEBUG_2_5_5 \
        && phpize \
        && ./configure \
        && make \
        && cp modules/xdebug.so /usr/lib/php/extensions/no-debug-non-zts-20090626 \
        && docker-php-ext-enable xdebug
    )

    PHPCONF="/usr/local/etc/php/conf.d/200-xdebug.ini"
    echo "xdebug.remote_enable=1" >> ${PHPCONF}
    echo "xdebug.remote_host=${HOST_IP}" >> ${PHPCONF}
    echo "xdebug.profiler_enable=0" >> ${PHPCONF}
    # ?XDEBUG_PROFILE
    echo "xdebug.profiler_enable_trigger=1" >> ${PHPCONF}
    echo "xdebug.profiler_output_dir=\"/v-share\"" >> ${PHPCONF}

    ## install composer
    printf "\033[0;32m > Installing Composer Packages ...\n"
    (cd /var/www/ && composer install --ignore-platform-reqs)
else
    printf "\033[0;32m > Installing Composer Packages ...\n"
    (cd /var/www/ && composer install --ignore-platform-reqs --no-dev)
fi

chmod 0777 -R /data/erply/

touch /docker/initialized
