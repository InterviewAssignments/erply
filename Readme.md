## Demo 

[![asciicast](https://asciinema.org/a/dJ46yna1FrImP0Zptwsx9HkCO.png)](https://asciinema.org/a/dJ46yna1FrImP0Zptwsx9HkCO)


## How To Setup
Hi Dear Reviewer, I`m pretty sure you are using ``` linux ```, and have ``` docker ``` and ```docker compose``` installed.

If So,
- goto project root; you can see Makefile there
- just type ```make up```

Note: It's open 8000 port on your host to server http request, if it's not desirable change ``` docker-compose.local.yml ``` file.
[click here to start add product](http://localhost:8000)


##

if there was any problem don't hesitate to contact me: naderi.payam@gmail.com
I have tried my best and I Obviously wants you to see it in action.


## The tasks
- Create Erply Demo Account
  - Use this url: https://erply.com/sign-up/?plan=free&su_source=erplymainpage
  - Write your name as the name field
  - Write “PHP trial-task” for the Business Name field

- Create an application with a  form
  - Purpose is to add products to Erply system.
  - It should use Erply API (See https://erply.com/api/ )
  - It should check if the product with the same name already exists and should not create if it exists.

- Move the API/business logic to a message broker
  - You can use RabbitMQ, Apache Kafka, Redis or anything you like
  - The purpose is so that the program can run in the background (big tasks)
  
