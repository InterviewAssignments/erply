<?php
namespace Erply\Recruiting\Actions;

use Erply\Recruiting\ErplyDefined;
use Erply\Recruiting\ViewModel\RendererPhp;
use Erply\Recruiting\Services\Clients\ErplyClient;
use Erply\Recruiting\Services\FlashMessage\FlashMessage;


class NewProductPage
{
    /** @var ErplyClient */
    protected $erply;


    /**
     * NewProductPage constructor.
     *
     * @param ErplyClient $erply @IoC /clients/ErplyClient
     */
    function __construct(ErplyClient $erply)
    {
        $this->erply = $erply;
    }


    /**
     * Display New Product Form To User
     *
     * // TODO CSRF Protection
     *         It's not that hard to implement; needs just a session handling
     *         I think priority of CSRF task is not that high because purpose of this application
     *         is on private network at least.
     *
     * @return string
     */
    function __invoke()
    {
        // Retrieve Products Group From Upstream Server
        //
        // TODO using cache for product groups
        $groups  = [];
        $sGroups = $this->erply->getProductGroups();
        foreach ( $sGroups->get('records') as $g ) {
            $groups[] = [
                'id'   => $g['productGroupID'],
                'name' => $g['name'],
            ];
        }


        // Flash Message, Error During Save Data ...
        //
        $flashMessages = ( new FlashMessage(ErplyDefined::FLASH_NEW_PRODUCT) )
            ->fetchMessages();


        // Render Output
        //
        $renderer = new RendererPhp;
        $output   = $renderer->capture(
            ERPLY_DIR_ROOT.'/../theme/new_product_page.php'
            , [
                'groups'         => $groups,
                'flash_messages' => $flashMessages,
            ]
        );

        return $output;
    }
}
