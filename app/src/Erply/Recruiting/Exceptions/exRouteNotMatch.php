<?php
namespace Erply\Recruiting\Exceptions;


class exRouteNotMatch
    extends \RuntimeException
{
    protected $code = 404;
}
