<?php
namespace Erply\Recruiting;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Poirot\Psr7\Response\PhpServer;

use Poirot\Http\Psr\ResponseBridgeInPsr;

use Poirot\Ioc\Container;
use Poirot\Ioc\Interfaces\Respec\iServicesProvider;

use Poirot\Router\Interfaces\iRoute;
use Poirot\Router\Interfaces\iRouterStack;

use Poirot\Std\ErrorStack;
use Poirot\Std\Type\StdTravers;

use Poirot\Stream\Psr\StreamBridgeInPsr;
use Poirot\Stream\Streamable\STemporary;

use Erply\Recruiting\Server\BuildServer;
use Erply\Recruiting\Server\ArgResolver;
use Erply\Recruiting\ViewModel\RendererPhp;
use Erply\Recruiting\Exceptions\exRouteNotMatch;


/**
 * This is just a demonstration follow of which a Real Framework Does,
 * and I know that it can be Improved a Lot, I did`nt wanted to use full stack framework,
 * so, I decided to use some simple packages and a dispatch follow to made this work.
 *
 * thank you for your consideration.
 */
class Server
    implements iServicesProvider
{
    protected const ACTION = 'action';

    /** @var Container */
    protected $ioc;


    /**
     * Server constructor.
     *
     * @param BuildServer $builder
     * @param Container   $IoC
     */
    function __construct(BuildServer $builder = null, Container $IoC = null)
    {
        // Give IoC Container
        //
        if ($IoC !== null)
            $this->ioc = $IoC;


        // Build Application with Builder Options
        //
        if ($builder !== null)
            $builder->build($this);

    }


    /**
     * Listen To Request and Execute Handlers
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    function run()
    {
        ErrorStack::handleException(function(\Throwable $e) {
            $this->_handleException($e);
        });


        ## Match Request
        #
        /** @var RequestInterface $request */
        $request  = $this->services()->get('HttpPsrRequest');
        $router  = $this->_router();

        if (! $matchRoute = $router->match($request) )
            throw new exRouteNotMatch('Route Not Match.');


        ## Attain Actions and Parameters
        #
        // route params include, Route Named Param and Executable Action.
        $params = $matchRoute->params();
        $params = StdTravers::of($params)->toArray();

        $action = $params[self::ACTION] ?? null;
        unset($params[self::ACTION]); // remove action itself from params


        ## Resolve Args. For Callable
        #
        $invokable = new ArgResolver( $this->services() );
        $invokable = $invokable->resolveActionInvokable($action, $params);

        $response  = call_user_func($invokable);
        if ( is_string($response) )
        {
            // Build Response From String
            //
            /** @var ResponseBridgeInPsr $response */
            $response = $this->services()->get('HttpPsrResponse')
                ->withBody(
                    new StreamBridgeInPsr( new STemporary($response) )
                );
        }

        if (! $response instanceof ResponseInterface)
            throw new \Exception(sprintf(
                'Application Server Allow Only [ResponseInterface] Result From Action Callable on (%s).'
                , $matchRoute->getName()
            ));


        ErrorStack::handleDone();


        $this->_sendResponse($response);
    }

    /**
     * Handle Match Request
     *
     * @param iRoute          $route
     * @param callable|string $callable Maybe a class name given as callable ClassInvoke::class
     *
     * @return $this
     */
    function handleRequest(iRoute $route, $callable)
    {
        $route->params()->set(self::ACTION, $callable);
        $this->_router()->add($route);

        return $this;
    }

    /**
     * Get Sapi Name
     *
     * @return string
     */
    function getSapiName()
    {
        // apache2handler|cli| ...
        return php_sapi_name();
    }


    // iServicesProvider Implementation:

    /**
     * Services Container
     *
     * @return Container
     */
    function services()
    {
        if (! $this->ioc )
            $this->ioc = new Container;


        return $this->ioc;
    }


    // ..

    /**
     * Send Response
     *
     * @param ResponseInterface $response
     */
    function _sendResponse(ResponseInterface $response)
    {
        PhpServer::_($response)->send();

        die; // for all is dying, even the dead.
    }


    /**
     * @return iRouterStack
     */
    protected function _router()
    {
        $router = $this->services()->get('Router');
        return $router;
    }

    /**
     * Display Simple Error Page On User Browser
     *
     * @param \Throwable $e
     *
     * @throws \Throwable
     * @return mixed|string
     */
    private function _handleException($e)
    {
        try {
            $renderer = new RendererPhp;
            $errPage  = $renderer->capture(
                ERPLY_DIR_ROOT.'/../theme/.error.page.php'
                , array('exception' => $e)
            );
        } catch(\Exception $ve) {
            ## throw exception if can't render template
            throw $e;
        }


        ## Build Response Object
        #
        /** @var ResponseBridgeInPsr $response */
        $response = $this->services()->get('HttpPsrResponse');
        $response = $response->withBody(
            new StreamBridgeInPsr(new STemporary($errPage))
        );

        $this->_sendResponse($response);
    }
}
