<?php
namespace Erply\Recruiting\Services\Clients\Exceptions;

use Poirot\ApiClient\Exceptions\Response\exServerAuthorizationDeniedAccess;


class exInvalidSession
    extends exServerAuthorizationDeniedAccess
{ }
