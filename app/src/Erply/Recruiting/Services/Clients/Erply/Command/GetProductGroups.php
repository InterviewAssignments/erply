<?php
namespace Erply\Recruiting\Services\Clients\Erply\Command;


class GetProductGroups
    extends aCommand
{
    use tSessionAware;


    function getRequest()
    {
        return 'getProductGroups';
    }
}
