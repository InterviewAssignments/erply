<?php
namespace Erply\Recruiting\Services\Clients\Erply\Command;

use Poirot\ApiClient\Interfaces\Request\iApiCommand;
use Poirot\ApiClient\Request\tCommandHelper;


class VerifyUser
    extends aCommand
    implements iApiCommand
{
    use tCommandHelper;

    protected $username;
    protected $password;


    /**
     * Constructor.
     *
     * @param string $username
     * @param string $password
     */
    function __construct($username, $password)
    {
        $this->username = (string) $username;
        $this->password = (string) $password;
    }


    // Attributes:

    function getRequest()
    {
        return 'verifyUser';
    }

    function getUsername()
    {
        return $this->username;
    }

    function getPassword()
    {
        return $this->password;
    }
}
