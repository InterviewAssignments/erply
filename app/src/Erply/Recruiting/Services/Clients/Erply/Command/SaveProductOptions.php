<?php
namespace Erply\Recruiting\Services\Clients\Erply\Command;

use Poirot\Std\ConfigurableSetter;
use Poirot\Std\Hydrator\HydrateGetters;


class SaveProductOptions
    extends ConfigurableSetter
    implements \IteratorAggregate
{
    protected $groupId;
    protected $name;
    protected $description;
    protected $longdesc;
    protected $manufacturerName;
    protected $netPrice;
    protected $taxFree;


    function getGroupId()
    {
        return $this->groupId;
    }

    function setGroupId($groupId)
    {
        $this->groupId = $groupId;
        return $this;
    }

    function getName()
    {
        return $this->name;
    }

    function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    function getDescription()
    {
        return $this->description;
    }

    function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    function getLongdesc()
    {
        return $this->longdesc;
    }

    function setLongdesc($longdesc)
    {
        $this->longdesc = $longdesc;
        return $this;
    }

    function getManufacturerName()
    {
        return $this->manufacturerName;
    }

    function setManufacturerName($manufacturerName)
    {
        $this->manufacturerName = $manufacturerName;
        return $this;
    }

    function getNetPrice()
    {
        return $this->netPrice;
    }

    function setNetPrice($netPrice)
    {
        $this->netPrice = $netPrice;
        return $this;
    }

    function getTaxFree()
    {
        return (int) $this->taxFree;
    }

    function setTaxFree($taxFree)
    {
        $this->taxFree = (bool) $taxFree;
        return $this;
    }


    // Implement Iterator Aggregate:

    /**
     * @inheritdoc
     */
    function getIterator()
    {
        $hyd = new HydrateGetters($this, [HydrateGetters::PROPERTY_CAMELCASE => true]);
        $hyd->setExcludeNullValues();

        return $hyd;
    }
}
